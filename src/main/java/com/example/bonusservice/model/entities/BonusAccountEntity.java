package com.example.bonusservice.model.entities;

import com.example.bonusservice.model.BonusLevel;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Objects;
import java.util.UUID;

@Entity
public class BonusAccountEntity {

    @Id
    private UUID id;
    private BigDecimal value;
    private BigDecimal accountedAmount;
    private BonusLevel level;
    private ZonedDateTime lastUpdateTimestamp;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getAccountedAmount() {
        return accountedAmount;
    }

    public void setAccountedAmount(BigDecimal accountedAmount) {
        this.accountedAmount = accountedAmount;
    }

    public BonusLevel getLevel() {
        return level;
    }

    public void setLevel(BonusLevel level) {
        this.level = level;
    }

    public ZonedDateTime getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(ZonedDateTime lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BonusAccountEntity that = (BonusAccountEntity) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
