package com.example.bonusservice.controller;

import com.example.bonusservice.model.dto.BonusTransactionDto;
import com.example.bonusservice.service.BonusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.util.UUID;

@RestController
public class BonusTransactionController {

    private final BonusService bonusService;

    @Autowired
    public BonusTransactionController(BonusService bonusService) {
        this.bonusService = bonusService;
    }

    @PutMapping(path = "transactions/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> executeTransaction(HttpServletRequest request, @PathVariable("id") UUID id, @RequestBody BonusTransactionDto transactionDto) {
        final var created = bonusService.executeTransaction(
                id,
                transactionDto.getFrom(),
                transactionDto.getAmount()
        );
        return created ? ResponseEntity.created(URI.create(request.getRequestURI())).build() : ResponseEntity.noContent().build();
    }

    @GetMapping(path = "transactions/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public BonusTransactionDto getTransaction(@PathVariable("id") UUID id) {
        return bonusService.findTransaction(id).map(entity -> {
            final var transactionDto = new BonusTransactionDto();
            transactionDto.setFrom(entity.getFrom());
            transactionDto.setAmount(entity.getAmount());
            return transactionDto;
        }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @DeleteMapping(path = "transactions/{id}")
    public ResponseEntity<Void> revertTransaction(@PathVariable("id") UUID id) {
        bonusService.revertTransaction(id);
        return ResponseEntity.noContent().build();
    }
}
