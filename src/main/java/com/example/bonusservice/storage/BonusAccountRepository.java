package com.example.bonusservice.storage;

import com.example.bonusservice.model.entities.BonusAccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface BonusAccountRepository extends JpaRepository<BonusAccountEntity, UUID> {

}
