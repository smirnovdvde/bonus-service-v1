package com.example.bonusservice.storage;

import com.example.bonusservice.model.entities.BonusTransactionEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface BonusTransactionRepository extends JpaRepository<BonusTransactionEntity, UUID> {

    Optional<BonusTransactionEntity> findByIdAndReverted(UUID id, boolean reverted);
}
