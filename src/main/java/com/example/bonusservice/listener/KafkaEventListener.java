package com.example.bonusservice.listener;

import com.example.bonusservice.service.BonusService;
import com.example.transactionservice.model.events.TransactionEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Component
@ConditionalOnProperty("transactionEvents.topicName")
public class KafkaEventListener {

    private final BonusService bonusService;

    @Autowired
    public KafkaEventListener(BonusService bonusService) {
        this.bonusService = bonusService;
    }

    @KafkaListener(topics = "${transactionEvents.topicName}")
    @Transactional
    public void onEvent(@Payload TransactionEvent event) {
        bonusService.onTransactionEvent(event);
    }
}

