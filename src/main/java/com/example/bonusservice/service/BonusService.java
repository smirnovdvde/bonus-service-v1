package com.example.bonusservice.service;

import com.example.bonusservice.model.BonusLevel;
import com.example.bonusservice.model.entities.BonusAccountEntity;
import com.example.bonusservice.model.entities.BonusTransactionEntity;
import com.example.bonusservice.storage.BonusAccountRepository;
import com.example.bonusservice.storage.BonusTransactionRepository;
import com.example.transactionservice.model.events.TransactionEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
public class BonusService {

    private final BonusAccountRepository bonusAccountRepository;
    private final BonusTransactionRepository bonusTransactionRepository;

    @Autowired
    public BonusService(BonusAccountRepository bonusAccountRepository, BonusTransactionRepository bonusTransactionRepository) {
        this.bonusAccountRepository = bonusAccountRepository;
        this.bonusTransactionRepository = bonusTransactionRepository;
    }

    @Transactional
    public void onTransactionEvent(TransactionEvent event) {
        if (BigDecimal.ZERO.compareTo(event.amount) <= 0) {
            return;
        }
        final var account = bonusAccountRepository.findById(event.id).orElseGet(() -> {
            final var newAccount = new BonusAccountEntity();
            newAccount.setId(event.id);
            newAccount.setAccountedAmount(BigDecimal.ZERO);
            newAccount.setLevel(defineLevel(newAccount.getAccountedAmount()));
            newAccount.setValue(BigDecimal.ZERO);
            newAccount.setLastUpdateTimestamp(ZonedDateTime.now());
            return newAccount;
        });
        if (event.reverted) {
            account.setAccountedAmount(account.getAccountedAmount().subtract(event.amount.negate()));
            account.setValue(account.getValue().subtract(defineBonusAmount(event.amount.negate())));
        } else {
            account.setAccountedAmount(account.getAccountedAmount().add(event.amount.negate()));
            account.setValue(account.getValue().add(defineBonusAmount(event.amount.negate())));
        }
        account.setLevel(defineLevel(account.getAccountedAmount()));
        account.setLastUpdateTimestamp(ZonedDateTime.now());
        bonusAccountRepository.save(account);
    }

    @Transactional
    public Optional<BonusAccountEntity> findAccount(UUID id) {
        return bonusAccountRepository.findById(id);
    }

    @Transactional
    public boolean executeTransaction(UUID id, UUID accountId, BigDecimal amount) {
        if (bonusTransactionRepository.existsById(id)) {
            return false;
        }
        final var entity = new BonusTransactionEntity();
        entity.setId(id);
        entity.setFrom(accountId);
        entity.setAmount(amount);
        entity.setTimestamp(ZonedDateTime.now());
        entity.setReverted(false);

        final var bonusAccountEntity = bonusAccountRepository.getById(accountId);
        if (bonusAccountEntity.getValue().compareTo(amount) < 0) {
            throw new IllegalArgumentException("incorrect amount");
        }
        bonusAccountEntity.setValue(bonusAccountEntity.getValue().subtract(amount));
        bonusAccountEntity.setLastUpdateTimestamp(ZonedDateTime.now());
        bonusAccountRepository.save(bonusAccountEntity);

        bonusTransactionRepository.save(entity);
        return true;
    }

    @Transactional
    public void revertTransaction(UUID id) {
        final var entityOpt = bonusTransactionRepository.findById(id);
        if (entityOpt.isPresent()) {
            final var entity = entityOpt.get();
            entity.setReverted(true);

            final var bonusAccountEntity = bonusAccountRepository.getById(entity.getFrom());
            bonusAccountEntity.setValue(bonusAccountEntity.getValue().add(entity.getAmount()));
            bonusAccountEntity.setLastUpdateTimestamp(ZonedDateTime.now());
            bonusAccountRepository.save(bonusAccountEntity);

            bonusTransactionRepository.save(entity);
        }
    }

    @Transactional
    public Optional<BonusTransactionEntity> findTransaction(UUID id) {
        return bonusTransactionRepository.findByIdAndReverted(id, false);
    }

    private static BonusLevel defineLevel(BigDecimal value) {
        if (BigDecimal.valueOf(1_000).compareTo(value) > 0) {
            return BonusLevel.Basic;
        } else if (BigDecimal.valueOf(10_000).compareTo(value) > 0) {
            return BonusLevel.Bronze;
        } else if (BigDecimal.valueOf(100_000).compareTo(value) > 0) {
            return BonusLevel.Silver;
        } else {
            return BonusLevel.Gold;
        }
    }

    private static BigDecimal defineBonusAmount(BigDecimal value) {
        return value.multiply(BigDecimal.valueOf(1, 2));
    }
}
